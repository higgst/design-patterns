package com.higgsup.internship.chain.of.responsibility;

/**
 * Created by hunghip on 7/27/2016.
 */
public class Cat extends Animal {

    public Cat(String cat) {
        this.animal = cat;
    }

    @Override
    protected void Running() {
        System.out.println("Meo meo meo");
    }
}
