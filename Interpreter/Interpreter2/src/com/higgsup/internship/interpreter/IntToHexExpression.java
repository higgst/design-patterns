package com.higgsup.internship.interpreter;

/**
 * Created by hunghip on 8/1/2016.
 */
public class IntToHexExpression implements Expression {

    private int i;

    public IntToHexExpression(int c){
        this.i=c;
    }

    @Override
    public String interpret(InterpreterContext ic) {
        return ic.getHexadecimalFormat(i);
    }

}