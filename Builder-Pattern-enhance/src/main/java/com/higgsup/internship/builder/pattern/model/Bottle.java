package com.higgsup.internship.builder.pattern.model;

import com.higgsup.internship.builder.pattern.interfaces.Packing;

/**
 * Created by hunghip on 7/11/2016.
 */
public class Bottle implements Packing {
    public String pack() {
        return "Bottle";
    }
}
