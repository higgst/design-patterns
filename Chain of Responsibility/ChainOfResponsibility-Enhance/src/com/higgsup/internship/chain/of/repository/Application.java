package com.higgsup.internship.chain.of.repository;

import com.higgsup.internship.chain.of.repository.chain.enryptor.AgeEncrypt;
import com.higgsup.internship.chain.of.repository.chain.enryptor.FirstNameEncrypt;
import com.higgsup.internship.chain.of.repository.chain.enryptor.LastNameEncrypt;
import com.higgsup.internship.chain.of.repository.chain.enryptor.PhoneNumberEncrypt;
import com.higgsup.internship.chain.of.repository.model.Check;
import com.higgsup.internship.chain.of.repository.model.User;
import com.higgsup.internship.chain.of.repository.user.details.encryptor.Encryptor;

import java.security.NoSuchAlgorithmException;

/**
 * Created by hunghip on 7/28/2016.
 */
public class Application {
    public static void main(String[] args) throws NoSuchAlgorithmException {

        User user = new User();
        user.setFirstName("le");
        user.setLastName("hung");
//        user.setAge("10");
        user.setPhoneNumber("09871213");

        Encryptor firstName = new FirstNameEncrypt();
        Encryptor lastName = new LastNameEncrypt();
        Encryptor age = new AgeEncrypt();
        Encryptor phoneNumber = new PhoneNumberEncrypt();

        firstName.setNextDetail(lastName);
        lastName.setNextDetail(age);
        age.setNextDetail(phoneNumber);
        phoneNumber.setNextDetail(null);

        Check check = new Check(1);
        firstName.Run(user,check);
    }
}
