package com.higgsup.internship.interpreter.interpret;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 8/3/2016.
 */
public class GermanTrans extends Expression {

    @Override
    public String i() {
        System.out.println("ich");
        return "ich";
    }

    @Override
    public String love() {
        System.out.println("liebe");
        return "liebe";
    }

    @Override
    public String you() {
        System.out.println("dich");
        return "dich";
    }

    @Override
    public String am() {
        System.out.println("bin");
        return "bin";
    }

    @Override
    public String a() {
        System.out.println("ein");
        return "ein";
    }

    @Override
    public String superman() {
        System.out.println("Übermensch");
        return "Übermensch";
    }

    @Override
    public String can() {
        System.out.println("kann");
        return "kann";
    }

    @Override
    public String swim() {
        System.out.println("schwimmen");
        return "schwimmen";
    }

    @Override
    public String play() {
        System.out.println("spielen");
        return "spielen";
    }

    @Override
    public String soccer() {
        System.out.println("Fußball");
        return "Fußball";
    }
}
