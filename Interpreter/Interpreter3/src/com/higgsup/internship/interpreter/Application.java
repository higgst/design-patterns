package com.higgsup.internship.interpreter;

import com.higgsup.internship.interpreter.expression.Expression;
import com.higgsup.internship.interpreter.interpret.MainInterpreter;

/**
 * Created by hunghip on 8/1/2016.
 */
public class Application {
    public static void main(String[] args) {
        Expression context = new MainInterpreter("58 in Hex");
        context.checkType(context.getInput());
    }
}
