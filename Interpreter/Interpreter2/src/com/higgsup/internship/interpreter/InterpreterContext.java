package com.higgsup.internship.interpreter;

/**
 * Created by hunghip on 8/1/2016.
**/
public class InterpreterContext {

    public String getBinaryFormat(int i){
        return Integer.toBinaryString(i);
    }

    public String getHexadecimalFormat(int i){
        return Integer.toHexString(i);
    }
}
