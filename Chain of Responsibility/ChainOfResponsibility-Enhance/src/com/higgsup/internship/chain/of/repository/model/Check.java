package com.higgsup.internship.chain.of.repository.model;

/**
 * Created by hunghip on 8/2/2016.
 */
public class Check {
    int check;

    public Check(int check) {
        this.check = check;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }
}
