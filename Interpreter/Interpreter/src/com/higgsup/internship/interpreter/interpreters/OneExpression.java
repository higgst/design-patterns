package com.higgsup.internship.interpreter.interpreters;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 7/29/2016.
 */
public class OneExpression  extends Expression {
    public String one() { return "I"; }
    public String four(){ return "IV"; }
    public String five(){ return "V"; }
    public String nine(){ return "IX"; }
    public int multiplier() { return 1; }
}