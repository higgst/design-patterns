package com.higgsup.internship.builder.pattern;

import com.higgsup.internship.builder.pattern.builder.PreBuilder;
import com.higgsup.internship.builder.pattern.model.User;

/**
 * Created by hunghip on 7/13/2016.
 */
public class Application {
    public static void main(String[] args) {
        PreBuilder preBuilder = new PreBuilder();
        User user = preBuilder.createUser();
        user.getDetails();
    }
}
