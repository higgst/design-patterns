package com.higgsup.internship.chain.of.repository.user.details.encryptor;

import com.higgsup.internship.chain.of.repository.model.Check;
import com.higgsup.internship.chain.of.repository.model.User;

import java.security.NoSuchAlgorithmException;

/**
 * Created by hunghip on 7/28/2016.
 */
public abstract class Encryptor {

    public abstract User Encrypt(User user, Check check) throws NoSuchAlgorithmException;
    private Encryptor nextDetail;

    public User Run(User user,Check check) throws NoSuchAlgorithmException {
        this.Encrypt(user, check);
        if (this.nextDetail != null && check.getCheck() == 1 ) {
            nextDetail.Run(user,check);
        }
        return user;
    }

    public void setNextDetail(Encryptor nextDetail){
        this.nextDetail = nextDetail;
    }
}
