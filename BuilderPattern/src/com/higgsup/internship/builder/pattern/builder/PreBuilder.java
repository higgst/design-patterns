package com.higgsup.internship.builder.pattern.builder;

import com.higgsup.internship.builder.pattern.model.User;

/**
 * Created by hunghip on 7/13/2016.
 */
public class PreBuilder {
    public User createUser(){
      User user = new UserBuilder("Hung", "Le").age(30)
                      .phone("0123465")
                      .address("Ha Noi")
                      .build();
        return user;
    }
}
