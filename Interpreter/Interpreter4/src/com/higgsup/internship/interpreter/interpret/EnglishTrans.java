package com.higgsup.internship.interpreter.interpret;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 8/1/2016.
 */
public class EnglishTrans extends Expression {

    @Override
    public String i() {
        System.out.println("toi");
        return "toi";
    }

    @Override
    public String love() {
        System.out.println("yeu");
        return "yeu";
    }

    @Override
    public String you() {
        System.out.println("ban");
        return "ban";
    }

    @Override
    public String am() {
        System.out.println("la");
        return "la";
    }

    @Override
    public String a() {
        System.out.println("mot");
        return "mot";
    }

    @Override
    public String superman() {
        System.out.println("sieu nhan");
        return "sieu nhan";
    }

    @Override
    public String can() {
        System.out.println("co the");
        return "co the";
    }

    @Override
    public String swim() {
        System.out.println("boi");
        return "boi";
    }

    @Override
    public String play() {
        System.out.println("choi");
        return "choi";
    }

    @Override
    public String soccer() {
        System.out.println("da bong");
        return "da bong";
    }
}
