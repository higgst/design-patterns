package com.higgsup.internship.builder.pattern.model;

import com.higgsup.internship.builder.pattern.builder.UserBuilder;

/**
 * Created by hunghip on 7/13/2016.
 */
public class User {
    private  String firstName; //required
    private  String lastName;  //required
    private  int age;          //optional
    private  String phone;     //optional
    private  String address;   //optional

    public User(UserBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.phone = builder.phone;
        this.address = builder.address;
    }

    public void getFirstName() {
        System.out.println(firstName);
    }

    public void getLastName() {
        System.out.println(lastName);
    }

    public void getAge() {
        System.out.println(age);
    }

    public void getPhone() {
        System.out.println(phone);
    }

    public void getAddress() {
        System.out.println(address);
    }

    public void getDetails() {
        System.out.println(firstName);
        System.out.println(lastName);
        System.out.println(age);
        System.out.println(phone);
        System.out.println(address);

    }

//    public User(String firstName, String lastName) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//    }
//
//    public User(String firstName, String lastName, int age) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//    }
//
//    public User(String firstName, String lastName, int age, String phone) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//        this.phone = phone;
//    }
//
//    public User(String firstName, String lastName, int age, String phone, String address) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//        this.phone = phone;
//        this.address = address;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
}
