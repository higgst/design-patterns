package com.higgsup.internship.interpreter.interpret;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 8/1/2016.
 */
public class MainInterpreter extends Expression {

    public MainInterpreter(String input){
        this.input = input;
    }


    @Override
    public String interpret(int input) {
        return "1";
    }
}
