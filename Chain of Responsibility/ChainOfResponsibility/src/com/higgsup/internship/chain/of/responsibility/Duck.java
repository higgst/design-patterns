package com.higgsup.internship.chain.of.responsibility;

/**
 * Created by hunghip on 7/27/2016.
 */
public class Duck extends Animal {

    public Duck(String duck) {
        this.animal = duck;
    }

    @Override
    protected void Running() {
        System.out.println("Quac quac quac");
    }
}
