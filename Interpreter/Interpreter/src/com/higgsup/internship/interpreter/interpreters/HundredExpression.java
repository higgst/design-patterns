package com.higgsup.internship.interpreter.interpreters;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 7/29/2016.
 */
public class HundredExpression extends Expression {
    public  String one() { return "C"; }
    public  String four(){ return "CD"; }
    public  String five(){ return "D"; }
    public  String nine(){ return "CM"; }
    public  int multiplier() { return 100; }
}