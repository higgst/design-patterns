package com.higgsup.internship.facade.pattern.model;

/**
 * Created by hunghip on 7/20/2016.
 */
public class User {
    public String userName;

    public int wallet;

    public User(String userName,int wallet){
        this.userName = userName;
        this.wallet = wallet;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public void getDetails() {
        System.out.println("Ten:"+ userName);
        System.out.println("Vi tien con lai:" + wallet);
    }
}
