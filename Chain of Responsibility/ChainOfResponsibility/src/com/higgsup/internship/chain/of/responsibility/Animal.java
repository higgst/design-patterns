package com.higgsup.internship.chain.of.responsibility;

/**
 * Created by hunghip on 7/27/2016.
 */
public abstract class Animal {

    static String DOG = "Dog";
    static String CAT = "Cat";
    static String DUCK = "Duck";

    public String animal;
    private Animal nextAnimal;
    protected abstract void Running();
    public void Run(String animal){
        if (animal == this.animal){
            Running();
        }
        else{
            if(nextAnimal != null)
            {
                nextAnimal.Run(animal);
            }
            else {
                System.out.println("No appropriate class");
            }
        }

    }
    public void SetNextProcess(Animal process)
    {
        nextAnimal = process;
    }
}
