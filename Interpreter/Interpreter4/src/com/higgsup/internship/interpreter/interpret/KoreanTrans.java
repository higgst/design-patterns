package com.higgsup.internship.interpreter.interpret;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 8/3/2016.
 */
public class KoreanTrans extends Expression{
    @Override
    public String i() {
        System.out.println("나는");
        return "나는";
    }

    @Override
    public String love() {
        System.out.println("애정");
        return "애정";
    }

    @Override
    public String you() {
        System.out.println("당신");
        return "당신";
    }

    @Override
    public String am() {
        System.out.println("오전");
        return "오전";
    }

    @Override
    public String a() {
        System.out.println("에이");
        return "에이";
    }

    @Override
    public String superman() {
        System.out.println("초인");
        return "초인";
    }

    @Override
    public String can() {
        System.out.println("양철통");
        return "양철통";
    }

    @Override
    public String swim() {
        System.out.println("수영하다");
        return "수영하다";
    }

    @Override
    public String play() {
        System.out.println("놀이");
        return "놀이";
    }

    @Override
    public String soccer() {
        System.out.println("축구");
        return "축구";
    }
}
