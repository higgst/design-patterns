package com.higgsup.internship.chain.of.responsibility;

/**
 * Created by hunghip on 7/27/2016.
 */
public class Application {
    public static void main(String [] args) {
        Animal dog = new Dog(Animal.DOG);
        Animal cat = new Cat(Animal.CAT);
        Animal duck = new Duck(Animal.DUCK);
        dog.SetNextProcess(cat);
        cat.SetNextProcess(duck);
        duck.SetNextProcess(null);
        dog.Run("Dog");
    }
}
