package com.higgsup.internship.facade.pattern.subsystem;

import com.higgsup.internship.facade.pattern.model.Product;
import com.higgsup.internship.facade.pattern.model.User;

/**
 * Created by hunghip on 7/18/2016.
 */
public class Payment {
    public User payMent(User user, Product product, int amount) {
        Integer total = product.getPrice()*amount;
        user.setWallet(user.getWallet()-total);
        return user;
    }
}
