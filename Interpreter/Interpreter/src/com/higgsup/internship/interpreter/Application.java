package com.higgsup.internship.interpreter;

import com.higgsup.internship.interpreter.context.Context;
import com.higgsup.internship.interpreter.expression.Expression;
import com.higgsup.internship.interpreter.interpreters.HundredExpression;
import com.higgsup.internship.interpreter.interpreters.OneExpression;
import com.higgsup.internship.interpreter.interpreters.TenExpression;
import com.higgsup.internship.interpreter.interpreters.ThousandExpression;

import java.util.ArrayList;

/**
 * Created by hunghip on 7/29/2016.
 */
public class Application {
    /**
     * @param args
     */
    public static void main(String[] args) {

        String roman = "MMMCDCCXXVIIII";
        Context context = new Context(roman);

        // Build the 'parse tree'
        ArrayList<Expression> tree = new ArrayList<Expression>();
        tree.add(new ThousandExpression());
        tree.add(new HundredExpression());
        tree.add(new TenExpression());
        tree.add(new OneExpression());

        // Interpret
        for (Expression expression : tree){
            expression.interpret(context);
        }

        System.out.println(roman + " = " + Integer.toString(context.getOutput()));
    }
}
