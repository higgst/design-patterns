package com.higgsup.internship.interpreter.interpreters;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 7/29/2016.
 */
public class TenExpression  extends Expression {
    public String one() { return "X"; }
    public String four(){ return "XL"; }
    public String five(){ return "L"; }
    public String nine(){ return "XC"; }
    public int multiplier() { return 10; }
}