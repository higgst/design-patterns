package com.higgsup.internship.interpreter;

/**
 * Created by hunghip on 8/1/2016.
 */
public interface Expression {
    String interpret(InterpreterContext ic);
}
