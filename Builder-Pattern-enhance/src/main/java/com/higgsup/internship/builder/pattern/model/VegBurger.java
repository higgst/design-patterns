package com.higgsup.internship.builder.pattern.model;

import com.higgsup.internship.builder.pattern.abstracts.Burger;

/**
 * Created by hunghip on 7/11/2016.
 */
public class VegBurger extends Burger {

    public float price() {
        return 25.0f;
    }

    public String name() {
        return "Veg Burger";
    }
}
