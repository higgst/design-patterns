package com.higgsup.internship.interpreter;

import com.higgsup.internship.interpreter.expression.Expression;
import com.higgsup.internship.interpreter.interpret.EnglishTrans;
import com.higgsup.internship.interpreter.interpret.KoreanTrans;
import com.higgsup.internship.interpreter.interpret.GermanTrans;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by hunghip on 8/1/2016.
 */
public class Application {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Context context = new Context();
        context.setInput("i love you");
        Expression engTrans = new EnglishTrans();
        Expression gerTrans = new GermanTrans();
        Expression koreanTrans = new KoreanTrans();
        koreanTrans.interpret(context);
    }
}
