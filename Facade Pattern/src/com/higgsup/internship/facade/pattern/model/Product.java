package com.higgsup.internship.facade.pattern.model;

/**
 * Created by hunghip on 7/20/2016.
 */
public class Product {
    public String productName;

    public int amount;

    public int price;

    public Product(String productName, int amount, int price){
        this.productName = productName;
        this.amount = amount;
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getProductName() {

        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
