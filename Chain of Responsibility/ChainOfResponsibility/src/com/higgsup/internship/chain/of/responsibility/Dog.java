package com.higgsup.internship.chain.of.responsibility;

/**
 * Created by hunghip on 7/27/2016.
 */
class Dog extends Animal
{
    public Dog(String dog) {
        this.animal = dog;
    }

    @Override
    protected void Running() {
        System.out.println("Gau gau gau");
    }

}
