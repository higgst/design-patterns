package com.higgsup.internship.facade.pattern.facade;

import com.higgsup.internship.facade.pattern.model.Product;
import com.higgsup.internship.facade.pattern.model.User;
import com.higgsup.internship.facade.pattern.subsystem.Inventory;
import com.higgsup.internship.facade.pattern.subsystem.Payment;
import com.higgsup.internship.facade.pattern.subsystem.Price;

/**
 * Created by hunghip on 7/18/2016.
 */
public class OrderFacade {
    private Payment payment = new Payment();
    private Inventory inventory = new Inventory();
    private Price price = new Price();

    public User placeOrder(User user,Product product,java.lang.String productName, int amount) {

        Boolean checkInventory = inventory.checkInventory(product, productName, amount);

        if (checkInventory == true){

            System.out.println("So luong hang con du");
            Boolean checkPrice = price.checkPrice(user, product, amount);

            if (checkPrice == true){
                System.out.println("Ban co du tien");
                payment.payMent(user, product, amount);
                System.out.println("Mua hang thanh cong");
                return user;
            }
            else System.out.println("Ban khong du tien");
        }
        else {
            System.out.println("Hang khong du");
            return user;
        }
        return user;
    }
}
