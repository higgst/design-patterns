package com.higgsup.internship.facade.pattern.subsystem;

import com.higgsup.internship.facade.pattern.model.Product;

/**
 * Created by hunghip on 7/18/2016.
 */
public class Inventory {
    public Boolean checkInventory(Product product, String productName, int amount) {
        if (amount<=product.getAmount()){
            return true;
        }
        else return false;
    }
}
