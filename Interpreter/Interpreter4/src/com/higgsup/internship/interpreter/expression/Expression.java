package com.higgsup.internship.interpreter.expression;

import com.higgsup.internship.interpreter.Context;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by hunghip on 8/1/2016.
 */
public abstract class Expression {

    public static String[] LIST = {"i","love","you","am","a","dog","superman","can","swim","play","soccer"};

    public abstract String  i();
    public abstract String love();
    public abstract String you();
    public abstract String am();
    public abstract String a();
    public abstract String superman();
    public abstract String can();
    public abstract String swim();
    public abstract String play();
    public abstract String soccer();

    public String context;

    public void interpret(Context input) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String context[] = input.getInput().split(" ");
        for (String context1: context){
            for (int i=0;i<=LIST.length;i++){
                if (context1.equals(LIST[i])){
                    String a = LIST[i] ;
                    java.lang.reflect.Method method = this.getClass().getMethod(a);
                    method.invoke(this);
                    break;
                }
            }
        }
    }
}
