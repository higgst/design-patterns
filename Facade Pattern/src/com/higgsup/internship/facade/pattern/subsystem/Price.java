package com.higgsup.internship.facade.pattern.subsystem;

import com.higgsup.internship.facade.pattern.model.Product;
import com.higgsup.internship.facade.pattern.model.User;

/**
 * Created by hunghip on 7/19/2016.
 */
public class Price {

    public Boolean checkPrice(User user, Product product, int amount) {
        int price = product.getPrice()*amount;
        if (user.getWallet()>=price){
            return true;
        }
        else return false;

    }
}
