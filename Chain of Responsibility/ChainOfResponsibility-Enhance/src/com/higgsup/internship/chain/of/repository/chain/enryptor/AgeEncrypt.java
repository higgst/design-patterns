package com.higgsup.internship.chain.of.repository.chain.enryptor;

import com.higgsup.internship.chain.of.repository.model.Check;
import com.higgsup.internship.chain.of.repository.model.User;
import com.higgsup.internship.chain.of.repository.user.details.encryptor.Encryptor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hunghip on 7/28/2016.
 */
public class AgeEncrypt extends Encryptor {
    @Override
    public User Encrypt(User user, Check check) throws NoSuchAlgorithmException {
        if (user.getAge() != null){
            String toEnc = user.getAge(); // Value to hash
            MessageDigest mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(toEnc.getBytes(), 0, toEnc.length());
            String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
            user.setAge(md5);
            System.out.println("Age: " + user.getAge());
        }
        return user;
    }
}
