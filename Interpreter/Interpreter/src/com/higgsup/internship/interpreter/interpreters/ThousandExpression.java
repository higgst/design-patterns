package com.higgsup.internship.interpreter.interpreters;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 7/29/2016.
 */
public class ThousandExpression  extends Expression {

    public String one() { return "M"; }
    public String four(){ return " "; }
    public String five(){ return " "; }
    public String nine(){ return " "; }
    public int multiplier() { return 1000; }
}