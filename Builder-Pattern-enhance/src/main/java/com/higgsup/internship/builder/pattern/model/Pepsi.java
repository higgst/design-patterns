package com.higgsup.internship.builder.pattern.model;

import com.higgsup.internship.builder.pattern.abstracts.ColdDrink;

/**
 * Created by hunghip on 7/11/2016.
 */
public class Pepsi extends ColdDrink {

    @Override
    public float price() {
        return 35.0f;
    }


    public String name() {
        return "Pepsi";
    }
}