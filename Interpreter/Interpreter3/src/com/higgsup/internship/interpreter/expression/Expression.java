package com.higgsup.internship.interpreter.expression;

import com.higgsup.internship.interpreter.interpret.IntoBinary;
import com.higgsup.internship.interpreter.interpret.IntoHex;

/**
 * Created by hunghip on 8/1/2016.
 */
public abstract class Expression {

    public String input;

    public abstract String interpret(int input);

    public void checkType(String input){
        if (input.contains("Binary")){
            Expression intoBinary = new IntoBinary();
            intoBinary.interpret(Integer.parseInt(input.substring(0,input.indexOf(" "))));
        }
        else if ( input.contains("Hex")){
            Expression intoHex = new IntoHex();
            intoHex.interpret(Integer.parseInt(input.substring(0,input.indexOf(" "))));
        }
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}