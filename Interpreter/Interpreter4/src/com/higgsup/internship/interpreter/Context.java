package com.higgsup.internship.interpreter;

/**
 * Created by hunghip on 8/1/2016.
 */
public class Context {
    public String input;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
