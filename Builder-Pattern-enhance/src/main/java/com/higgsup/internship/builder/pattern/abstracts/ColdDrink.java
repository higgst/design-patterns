package com.higgsup.internship.builder.pattern.abstracts;

import com.higgsup.internship.builder.pattern.model.Bottle;
import com.higgsup.internship.builder.pattern.interfaces.Item;
import com.higgsup.internship.builder.pattern.interfaces.Packing;

/**
 * Created by hunghip on 7/11/2016.
 */
public abstract class ColdDrink implements Item {

    public Packing packing() {
        return new Bottle();
    }

    public abstract float price();
}
