package com.higgsup.internship.facade.pattern;

import com.higgsup.internship.facade.pattern.facade.OrderFacade;
import com.higgsup.internship.facade.pattern.model.Product;
import com.higgsup.internship.facade.pattern.model.User;

/**
 * Created by hunghip on 7/18/2016.
 */
public class Application {
    public static void main(String args[]){
        OrderFacade orderFacade = new OrderFacade();
        User user = new User("Hung", 100);
        Product product = new Product("Ao dai tay", 5, 20);
        orderFacade.placeOrder(user, product,"Ao dai tay", 5);
        user.getDetails();
    }
}
