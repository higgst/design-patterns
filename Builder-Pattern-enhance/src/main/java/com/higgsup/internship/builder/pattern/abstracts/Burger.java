package com.higgsup.internship.builder.pattern.abstracts;

import com.higgsup.internship.builder.pattern.model.Wrapper;
import com.higgsup.internship.builder.pattern.interfaces.Item;
import com.higgsup.internship.builder.pattern.interfaces.Packing;

/**
 * Created by hunghip on 7/11/2016.
 */
public abstract class Burger implements Item {

    public Packing packing() {
        return new Wrapper();
    }

    public abstract float price();

    public abstract String name();
}