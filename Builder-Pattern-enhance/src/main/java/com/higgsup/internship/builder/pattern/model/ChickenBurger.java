package com.higgsup.internship.builder.pattern.model;

import com.higgsup.internship.builder.pattern.abstracts.Burger;

/**
 * Created by hunghip on 7/11/2016.
 */
public class ChickenBurger extends Burger {

    @Override
    public float price() {
        return 50.5f;
    }

    public String name() {
        return "Chicken Burger";
    }
}
