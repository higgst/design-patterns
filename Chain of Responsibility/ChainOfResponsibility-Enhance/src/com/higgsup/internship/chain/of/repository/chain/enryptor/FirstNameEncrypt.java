package com.higgsup.internship.chain.of.repository.chain.enryptor;

import com.higgsup.internship.chain.of.repository.model.Check;
import com.higgsup.internship.chain.of.repository.model.User;
import com.higgsup.internship.chain.of.repository.user.details.encryptor.Encryptor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hunghip on 7/28/2016.
 */
public class FirstNameEncrypt extends Encryptor {
    @Override
    public User Encrypt(User user, Check check) throws NoSuchAlgorithmException {
        if (user.getFirstName() != null){
            String toEnc = user.getFirstName(); // Value to hash
            MessageDigest mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(toEnc.getBytes(), 0, toEnc.length());
            String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
            user.setFirstName(md5);
            System.out.println("First Name: " + user.getFirstName());
        }
        return user;
    }
}
