package com.higgsup.internship.interpreter.interpret;

import com.higgsup.internship.interpreter.expression.Expression;

/**
 * Created by hunghip on 8/1/2016.
 */
public class IntoBinary extends Expression {
    @Override
    public String interpret(int input) {
        System.out.println(Integer.toBinaryString(input));
       return Integer.toBinaryString(input);
    }
}
