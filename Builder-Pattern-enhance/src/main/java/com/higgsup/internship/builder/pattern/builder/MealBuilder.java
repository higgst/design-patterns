package com.higgsup.internship.builder.pattern.builder;

import com.higgsup.internship.builder.pattern.*;
import com.higgsup.internship.builder.pattern.model.ChickenBurger;
import com.higgsup.internship.builder.pattern.model.Coke;
import com.higgsup.internship.builder.pattern.model.Pepsi;
import com.higgsup.internship.builder.pattern.model.VegBurger;

/**
 * Created by hunghip on 7/11/2016.
 */
public class MealBuilder {

    public Meal prepareVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }

    public Meal prepareNonVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Pepsi());
        return meal;
    }
}
