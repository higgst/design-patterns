package com.higgsup.internship.builder.pattern.interfaces;

/**
 * Created by hunghip on 7/11/2016.
 */
public interface Item {
    public String name();
    public Packing packing();
    public float price();
}
